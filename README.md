# OpenCV with NodeJS

### Usage

1. You can pull the image directly from DockerHub by using below command
	* docker pull saibabanadh/opencv-nodejs

2. You can build your own image using Dockerfile in this repository
	*   a. clone this repository.
	* 	b. open this repository from docker client
	* 	c. run below command
	 		docker build -t <your-image-name> .
		

### Included softwares:

* Base Operating System: Ubuntu:14.04
* opencv packages
* nodejs 5.xx
* npm 3.xx
* git
* nano
* wget
* unzip